/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   LCD显示英文
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火 F103-MINI STM32 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 
 
#include "stm32f10x.h"
#include "./lcd/bsp_ili9341_lcd.h"
#include "./usart/bsp_usart.h" 
#include <stdio.h>
#include "./usart/rx_data_queue.h"
#include "bsp_SysTick.h"

static void LCD_Test(void);	
static void Delay ( __IO uint32_t nCount );
void Printf_Charater(void)   ;
void LCD_show(void);

uint8_t CH2O_send_cmd1[] = {0xff, 0x01, 0x78, 0x41, 0x00, 0x00, 0x00, 0x00, 0x46};
uint8_t CH2O_send_cmd2[] = {0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};

/**
  * @brief  主函数
  * @param  无  
  * @retval 无
  */
int main ( void )
{
    SysTick_Init();

	USART_Config();		
	
	ILI9341_Init ();         //LCD 初始化

	rx_queue_init();
	
	printf("\r\n ********** 液晶屏英文显示程序*********** \r\n"); 
	printf("\r\n 本程序不支持中文，显示中文的程序请学习下一章 \r\n"); 
	
	//其中0、3、5、6 模式适合从左至右显示文字，
    //不推荐使用其它模式显示文字	其它模式显示文字会有镜像效果			
    //其中 6 模式为大部分液晶例程的默认显示方向  
    ILI9341_GramScan ( 6 );
	LCD_Test();
	
	Delay_ms(10*1000UL);
//	Delay(10*1000UL);
	Usart_SendArray( DEBUG_USARTx, CH2O_send_cmd1, 9);
	Delay_ms(1000);
//	Delay(1000UL);
	LCD_ClearLine(LINE(0));	/* 清除单行文字 */
	
	while ( 1 )
	{
		Usart_SendArray( DEBUG_USARTx, CH2O_send_cmd2, 9);
        LCD_show();
		pull_data_from_queue();
		Delay_ms(1000);
//		Delay(1000UL);
	}		
}

/*用于测试各种液晶的函数*/
void LCD_Test(void)
{
	/*演示显示变量*/
	static uint8_t testCNT = 0;	
	
	testCNT++;	
	
	LCD_SetFont(&Font8x16);
	LCD_SetColors(BLACK,WHITE);

    ILI9341_Clear(0,0,LCD_X_LENGTH,LCD_Y_LENGTH);	/* 清屏，显示全黑 */
	/********显示字符串示例*******/
    ILI9341_DispStringLine_EN(LINE(0),"System loading......");
	
}

float CH2O_data = 0;
float CH2O_ppm = 0;
extern volatile uint32_t running_time;

uint16_t tx_num = 0;
uint16_t rx_num = 0;
uint16_t rx_valid = 0;

void LCD_show(void)
{
    char dispBuff[100];
	
	/********显示变量示例*******/
	LCD_SetFont(&Font16x24);
	LCD_SetTextColor(BLUE);
	
	/*使用c标准库把变量转化成字符串*/
	sprintf(dispBuff,"TIME:%d s",running_time/1000);
    LCD_ClearLine(LINE(2));	/* 清除单行文字 */
	
	/*然后显示该字符串即可，其它变量也是这样处理*/
	ILI9341_DispStringLine_EN(LINE(2),dispBuff);
	
	LCD_SetFont(&Font16x24);
	LCD_SetTextColor(BLACK);
	
	/*使用c标准库把变量转化成字符串*/
	sprintf(dispBuff,"TX_NUM  :%d",tx_num);
    LCD_ClearLine(LINE(4));	/* 清除单行文字 */
	
	/*然后显示该字符串即可，其它变量也是这样处理*/
	ILI9341_DispStringLine_EN(LINE(4),dispBuff);
	
	LCD_SetFont(&Font16x24);
	LCD_SetTextColor(BLACK);
	
	/*使用c标准库把变量转化成字符串*/
	sprintf(dispBuff,"RX_NUM  :%d",rx_num);
    LCD_ClearLine(LINE(5));	/* 清除单行文字 */
	
	/*然后显示该字符串即可，其它变量也是这样处理*/
	ILI9341_DispStringLine_EN(LINE(5),dispBuff);
	
	LCD_SetFont(&Font16x24);
	LCD_SetTextColor(BLACK);
	
	/*使用c标准库把变量转化成字符串*/
	sprintf(dispBuff,"RX_VALID:%d",rx_num);
    LCD_ClearLine(LINE(6));	/* 清除单行文字 */
	
	/*然后显示该字符串即可，其它变量也是这样处理*/
	ILI9341_DispStringLine_EN(LINE(6),dispBuff);
	
	LCD_SetFont(&Font16x24);
	LCD_SetTextColor(BLACK);

	/*使用c标准库把变量转化成字符串*/
	sprintf(dispBuff,"CON:%-.3f mg/m3",CH2O_data);
    LCD_ClearLine(LINE(8));	/* 清除单行文字 */
	
	/*然后显示该字符串即可，其它变量也是这样处理*/
	ILI9341_DispStringLine_EN(LINE(8),dispBuff);
	
	sprintf(dispBuff,"PPM:%-.3f ppm",CH2O_ppm);
    LCD_ClearLine(LINE(9));	/* 清除单行文字 */
	
	/*然后显示该字符串即可，其它变量也是这样处理*/
	ILI9341_DispStringLine_EN(LINE(9),dispBuff);
	
	LCD_SetFont(&Font16x24);
	LCD_SetTextColor(GREEN);
	
	if((CH2O_data < 0.02) && (CH2O_ppm < 0.02))
	{
	    /*使用c标准库把变量转化成字符串*/
	    sprintf(dispBuff,"Perfect!");
        LCD_ClearLine(LINE(11));	/* 清除单行文字 */
	
	    /*然后显示该字符串即可，其它变量也是这样处理*/
	    ILI9341_DispStringLine_EN(LINE(11),dispBuff);
	}
	else
	{
	    if(((CH2O_data >= 0.02) && (CH2O_data < 0.05)) || ((CH2O_ppm >= 0.02) && (CH2O_ppm < 0.05)))
		{
		    /*使用c标准库把变量转化成字符串*/
	        sprintf(dispBuff,"Good!");
            LCD_ClearLine(LINE(11));	/* 清除单行文字 */
	
	        /*然后显示该字符串即可，其它变量也是这样处理*/
	        ILI9341_DispStringLine_EN(LINE(11),dispBuff);
		}
		else
		{
		    if(((CH2O_data >= 0.05) && (CH2O_data < 0.08)) || ((CH2O_ppm >= 0.05) && (CH2O_ppm < 0.08)))
		    {
			    /*使用c标准库把变量转化成字符串*/
	            sprintf(dispBuff,"Qualified!");
                LCD_ClearLine(LINE(11));	/* 清除单行文字 */
	
	            /*然后显示该字符串即可，其它变量也是这样处理*/
	            ILI9341_DispStringLine_EN(LINE(11),dispBuff);
		    }
			else
			{
				if((CH2O_data >= 0.08) || (CH2O_ppm >= 0.08))
				{
				    LCD_SetFont(&Font16x24);
	                LCD_SetTextColor(RED);

				    /*使用c标准库把变量转化成字符串*/
	                sprintf(dispBuff,"Bad!");
                    LCD_ClearLine(LINE(11));	/* 清除单行文字 */
	
	                /*然后显示该字符串即可，其它变量也是这样处理*/
	                ILI9341_DispStringLine_EN(LINE(11),dispBuff);
				}
			}
		}
	}
}


/**
  * @brief  简单延时函数
  * @param  nCount ：延时计数值
  * @retval 无
  */	
static void Delay ( __IO uint32_t nCount )
{
  for ( ; nCount != 0; nCount -- );
	
}

/* ------------------------------------------end of file---------------------------------------- */

